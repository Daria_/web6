//проработать choose!

window.addEventListener('DOMContentLoaded', function() {
            const PRICE1 = 300;
            const PRICE2 = 500;
            const PRICE3 = 800;
            const RAD1 = 200;
            const RAD2 = 500;
            const RAD3 = 1000;
            const CH = 100;

            let price = 0;
            let extraPrice;
            let res;

            let myselect = document.getElementById("myselect");
            let radiosDiv = document.getElementById("radios");
            let checkboxesDiv = document.getElementById("checkboxes");
            let radios = document.querySelectorAll("#radios label input[type=radio]"); //путь
            let checkboxes = document.querySelectorAll("#checkboxes label input[type=checkbox]");
            let choose1 = document.getElementById("choose1");
            let choose2 = document.getElementById("choose2");
            let choose3 = document.getElementById("choose3");
            let num = document.getElementById("num");
            let calculator = document.getElementById("calculator");
            let result = document.getElementById("result");

            choose1.addEventListener("click", function(){
                myselect.value = "1";
                radiosDiv.classList.add("d-none");
                checkboxesDiv.classList.add("d-none");
                extraPrice = 0;
                price = PRICE1;
            });

            choose2.addEventListener("click", function(){
                myselect.value = "2";
                radiosDiv.classList.add("d-none");
                checkboxesDiv.classList.remove("d-none");
                extraPrice = 0;
                price = PRICE2;
            });

            choose3.addEventListener("click", function(){
                myselect.value = "3";
                radiosDiv.classList.remove("d-none");
                checkboxesDiv.classList.add("d-none");
                document.getElementById("rad1").checked = true;
                extraPrice = RAD1;
                price = PRICE3;
            });

            myselect.addEventListener("change", function(event) {
                    let op = event.target;
                    if (op.value === "1") {
                        radiosDiv.classList.add("d-none");
                        checkboxesDiv.classList.add("d-none");
                        extraPrice = 0;
                        price = PRICE1;
                    }
                    if (op.value === "2") {
                        radiosDiv.classList.add("d-none");
                        checkboxesDiv.classList.remove("d-none");
                        price = PRICE2;
                    }
                    if (op.value === "3") {
                        radiosDiv.classList.remove("d-none");
                        checkboxesDiv.classList.add("d-none");
                        document.getElementById("rad1").checked = true;
                        extraPrice = RAD1;
                        price = PRICE3;
                    }
                });
                
                checkboxes.forEach(function(currentCheckbox){
                    currentCheckbox.addEventListener("change", function(event){
                        let ch1 = document.getElementById("check1").checked;
                        let ch2 = document.getElementById("check2").checked;
                        let check = event.target;
                        if((ch1 === true && ch2 === false) || (ch1 === false && ch2 === true)){
                            extraPrice = CH;
                        }
                        else if (ch1 === true && ch2 === true){
                            extraPrice = CH*2;
                        }
                        else {
                            extraPrice = 0;
                        }
                    });
                });

                radios.forEach(function(currentRadio) {
                    currentRadio.addEventListener("change", function(event) {
                        // Выбранная радиокнопка
                        let radio = event.target;
                        // Устанавливаем надбавку в зависимости от радиокнопки
                        if (radio.value === "1") {
                            extraPrice = RAD1;
                        }
                        if (radio.value === "2") {
                            extraPrice = RAD2;
                        }
                        if (radio.value === "3") {
                            extraPrice = RAD3;
                        }
                    });
                });

                calculator.addEventListener("change", function(){
                    if(num.value < 0) {
                        num.value = 1;
                    }
                    res = (price + extraPrice)*num.value;
                    result.innerHTML = res;
                });

            });
